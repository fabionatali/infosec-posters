%% MAIN/MISC IMPORTS
\documentclass[fontsize=32pt]{scrartcl}
\usepackage[a2paper, margin=3cm, top=5cm, bottom=12cm]{geometry}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[none]{hyphenat}
\usepackage[document]{ragged2e}
\usepackage{graphicx}
\usepackage{caption}

%% CUSTOM COLOURS
\usepackage{xcolor}
\definecolor{gray}{RGB}{40, 40, 40}

%% FONTS
\usepackage{fontspec}
\usepackage{fontawesome5}
\setmainfont[BoldFont={Roboto Medium}]{Roboto Light}
\setmonofont{inconsolata}

%% TIKZ
\usepackage{tikz}
\usetikzlibrary{positioning}
\usepackage{tikzpagenodes}

%% PAGE NUMBERING
\pagestyle{empty}

%% PAGE TITLES
\renewcommand{\title}[1]{
  {\fontsize{64pt}{64pt}\selectfont\textbf{#1}\par}\vspace{2cm}
}

%% URLS
\usepackage[T1,hyphens]{url}
\usepackage[colorlinks,urlcolor=gray]{hyperref}

%% PARAGRAPHS
\setlength{\parskip}{.8cm} % distance between paragraphs
\usepackage{setspace}
\setstretch{1.2} % line height

%% LISTS
\usepackage{enumitem}
\setlist{noitemsep}
\setlist[enumerate, 1]{start=0, leftmargin=*}

%% LISTS IN A FRAME
\usepackage[framemethod=tikz]{mdframed}
\mdfdefinestyle{resourceliststyle}{%
  topline=false,%
  bottomline=false,%
  linewidth=3mm,%
  linecolor=gray!20,%
  innerrightmargin=10mm,%
  innerleftmargin=10mm,%
  leftmargin=-5mm,%
  rightmargin=-5mm,%
  skipabove=15mm,%
  skipbelow=15mm,%
  backgroundcolor=gray!10,%
  roundcorner=0%
}
\newenvironment{resourcelist}{
  \begin{mdframed}[style=resourceliststyle]
    \begin{minipage}[c]{\textwidth}
      \begin{itemize}[leftmargin=0pt]
}
{
      \end{itemize}
    \end{minipage}
  \end{mdframed}
}

% HEADER AND FOOTER
\usepackage{textpos}[absolute]
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{} % reset header and footer
\renewcommand{\headrulewidth}{0pt}
\rfoot{
  \begin{tikzpicture}[overlay, remember picture, inner sep=0pt, outer sep=0pt]
    \node (logo) [below=2cm, anchor=north west] at (current page text area.south west) {
      \includegraphics[width=7cm]{img/logo_cryptoparty.pdf}
    };
    \node (colophon) [below=of logo.south west, anchor=north west, align=left] {
      \scriptsize{{\tiny\color{gray!80}\faTwitter}~@CryptoPartyLDN} \\[-.3cm]
      \scriptsize{\url{https://www.cryptoparty.in/london/}} \\[-.3cm]
      \tiny{Creative Commons Attribution 4.0 International License}
    };
    \node (cameracaption) [base right=of colophon, anchor=south, xshift=19cm, yshift=-1mm]  {
      \tiny{\color{gray}You can take a photo!}
    };
    \node (camera) [above=5mm of cameracaption.north, anchor=south] {
      \large{\color{gray!80}\faCamera}
    };
  \end{tikzpicture}
}

\begin{document}
\raggedright

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{General recommendations}

Things you may want to ask us about

\begin{enumerate}
  \item How to enable \textbf{disk encryption}
  \item How to \textbf{securely delete} a file
  \item \textbf{Metadata} in files and communications
  \item \textbf{Opsec}, evil maid attacks, keyloggers, mics and cams
  \item Crossing a \textbf{border} while carrying sensitive data
  \item Social media \textbf{privacy settings}
  \item \textbf{Free/Open} vs proprietary software
  \item Phishing, malware, and other \textbf{threats}
  \item Your \textbf{threat-model}
\end{enumerate}

Online resources: \textbf{\textit{Security in-a-box}} and
\textbf{\textit{Me \& My Shadow}} (Tactical Tech);
\textbf{\textit{Surveillance Self-Defense}} (EFF); \textbf{\textit{A
    first look at digital security}} (Access Now);
\textbf{\textit{Infosec Bytes}} (CIJ).

\begin{resourcelist}
  \item[] \url{https://ssd.eff.org}
  \item[] \url{https://securityinabox.org}
  \item[] \url{https://myshadow.org}
  \item[] \url{http://infosecbytes.org}
  \item[] \url{https://www.accessnow.org/first-look-at-digital-security}
\end{resourcelist}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage

\title{Internet browsing and Tor}

\textbf{General browsing} / An \textbf{updated} version of Chrome or
Firefox with only a restricted selection of security and privacy
addons such as \textbf{NoScript}, \textbf{HTTPS Everywhere},
\textbf{uBlock Origin}, and \textbf{Privacy Badger}.

\textbf{Additional security and privacy} / Use the \textbf{Tor
  Browser}. Tor encrypts your traffic in multiple layers and routes it
via multiple hops to destination. Tor protects your \textbf{privacy}
and defends you against \textbf{censorship}.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.7\textwidth]{img/tor_diagram.pdf}
\end{figure}

\begin{resourcelist}
  \item[] \url{https://github.com/gorhill/uBlock}
  \item[] \url{https://www.eff.org/privacybadger}
  \item[] \url{https://www.eff.org/https-everywhere}
  \item[] \url{https://noscript.net}
  \item[] \url{https://torproject.org/}
\end{resourcelist}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage

\title{Encrypted communications}

\textbf{Messaging mobile} / \textbf{Signal} or \textbf{Wire} (over
Whatsapp and Telegram, ask us why) on an updated version of Android
and iOS

\textbf{Messaging desktop} / \textbf{Signal} also has a desktop
version; also consider Pidgin with the OTR plugin

\textbf{Encrypted email (and files)} / \textbf{Gnupg} + Thunderbird +
Enigmail; in alternative, Gnupg + Cut\&Paste + Webmail; NB configure
it properly - ask us for details

\textbf{SecureDrop} / a whistleblower submission system that media
organizations can install to securely accept documents from anonymous
sources

\begin{resourcelist}
  \item[] \url{https://signal.org}
  \item[] \url{https://wire.com}
  \item[] \url{https://www.mozilla.org/en-US/thunderbird}
  \item[] \url{https://gnupg.org}
  \item[] \url{https://www.enigmail.net}
  \item[] \url{https://securedrop.org}
\end{resourcelist}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage

\title{Passwords}

How to create a \textbf{strong password}, the diceware method.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.5\textwidth]{img/password_strength_xkcd.png}
  \caption*{\tiny{(By XKCD, see https://xkcd.com/936/ for licence)}}
\end{figure}

Do not reuse your passwords across multiple accounts. You may want to
use a password manager. Where possible, enable \textbf{two-factor or
  multi-factor authentication} (2FA or MFA).

\begin{resourcelist}
  \item[] \url{https://keepassxc.org}
  \item[] \url{https://ssd.eff.org/en/module/creating-strong-passwords}
  \item[] \url{https://theintercept.com/2015/03/26/passphrases-can-memorize-attackers-cant-guess}
  \item[] \url{https://twofactorauth.org}
x\end{resourcelist}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage

\title{Operating Systems}

\textbf{General recommendations} / Regular \textbf{updates} and
\textbf{backups}; hard-disk \textbf{encryption};
\textbf{compartmentalisation}.

\textbf{Alternatives OSes} / Consider using a Linux based OS as an
alternative to Mac OS X and Windows; free/open source vs proprietary
software

\textbf{Security focused OSes} / When extra security and privacy are
needed, consider \textbf{Tails} or \textbf{Qubes}

\begin{resourcelist}
  \item[] \url{https://tails.boum.org}
  \item[] \url{https://www.qubes-os.org}
\end{resourcelist}

\end{document}
